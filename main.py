from PIL import ImageTk, Image
import Tkinter as Tk
import spc
import tkFileDialog
import tkMessageBox
from shutil import copyfile

# Checks for valid imput variables and then calls the drawPlot function to generate a new set of control plots.
def refreshPlot():
    global specLimitVar
    
    if specLimitVar.get() == 0:
        spc.upperSpec = False
        spc.lowerSpec = False
        if sampleRangeVar.get() == 1:
            if upperSampleRange.get() > lowerSampleRange.get():
                spc.upperSampleRange = upperSampleRange.get()
                spc.lowerSampleRange = lowerSampleRange.get()
                drawPlot()
            else:
                tkMessageBox.showerror('Range Limit Error', 'Upper Range must be larger than Lower Range')
        else:
            spc.upperSampleRange = 0
            spc.lowerSampleRange = 0
            drawPlot() 
    
    else:          
        if specLimitVar.get() ==1:
            if USL.get() < LSL.get():
                tkMessageBox.showerror('Spec Limit Error', 'Upper Spec Limit cannot be larger than the Lower Spec Limit.')
                
            else:
                spc.upperSpec = USL.get()
                spc.lowerSpec = LSL.get()
                if sampleRangeVar.get() == 0:
                    if upperSampleRange.get() > lowerSampleRange.get():
                        spc.upperSampleRange = upperSampleRange.get()
                        spc.lowerSampleRange = lowerSampleRange.get()
                        drawPlot()
                    else:
                        tkMessageBox.showerror('Range Limit Error', 'Upper Range must be larger than Lower Range')
                    
                else:
                    spc.upperSampleRange = 0
                    spc.lowerSampleRange = 0
                    drawPlot() 
        


# Function generates and plots a set of control plots.
def drawPlot():
    global specLimitVar
    global controlLimitVar
    global sampleRangeVar
    global panel
    
    spc.runVars()    
    spc.z = zValue.get()
    
    if controlLimitVar.get() == 0:
        spc.MeanUCL = False
        spc.MeanLCL = False
        spc.RangeUCL = False
    if controlLimitVar.get() == 1:
        spc.runControl()
        
    spc.runPlots()
    
    panel.pack_forget()
    img = ImageTk.PhotoImage(Image.open("workingFile.png"))
    panel = Tk.Label(root, image = img)
    panel.grid(row=0,column=0, columnspan=20)
    panel.draw()

# Removes the current plot from the screen and replaces it with the default screen.
def clearPlot():

    global panel
    panel.pack_forget()
    img = ImageTk.PhotoImage(Image.open("DefaultScreen.png"))
    panel = Tk.Label(root, image = img)
    panel.grid(row=0,column=0, columnspan=20)
    panel.draw()

# Opens a .csv file and plots the file.
def openFile():
    
    file = tkFileDialog.askopenfile(parent=root,mode='rb',title='Choose a file')
    spc.data = spc.calculator.getArray(file)
    refreshPlot()

# Saves a .png image of the last control plot on the screen.
def saveFile():
    
    fileName = tkFileDialog.asksaveasfilename(parent=root, defaultextension=".png",title="Save the image as...")
    copyfile('workingFile.png', fileName)

# Opens the README fine in a message window.
def instructions():
    
    tkMessageBox.showinfo("SPC Analysis 1.0 Help", open("README.txt").read())
    

# Opens the contributors file in a message window.
def about():
    
    tkMessageBox.showinfo("About SPC Analysis 1.0", open("contributors.txt").read())
    
# Creates the root GUI window
root = Tk.Tk()
root.wm_title("SPC Analysis")

# Defines the working variables needed for the GUI objects.
specLimitVar = Tk.IntVar()
controlLimitVar = Tk.IntVar()
sampleRangeVar = Tk.IntVar()
upperSampleRange = Tk.IntVar()
lowerSampleRange = Tk.IntVar()
USL = Tk.DoubleVar()
LSL = Tk.DoubleVar()
zValue = Tk.DoubleVar(root, 1.5)

# Creates the menu bar
menubar = Tk.Menu(root)
filemenu = Tk.Menu(menubar, tearoff = 0)
filemenu.add_command(label = "Open", command = openFile)
filemenu.add_command(label = "Save", command = saveFile)
filemenu.add_separator()
filemenu.add_command(label = "Exit", command=root.quit)
menubar.add_cascade(label = "File", menu=filemenu)

helpmenu = Tk.Menu(menubar, tearoff = 0)
helpmenu.add_command(label = "Instructions", command = instructions)
helpmenu.add_command(label = "About", command = about)
menubar.add_cascade(label = "Help", menu=helpmenu)

root.config(menu=menubar)

# Creates the Check Boxes for user input.
specLimitsBox = Tk.Checkbutton(root, text="Plot With Spec Limits", variable=specLimitVar ,onvalue=1, offvalue=0)
controlLimitsBox = Tk.Checkbutton(root, text="Plot With Control Limits", variable=controlLimitVar ,onvalue=1, offvalue=0)
sampleRangeBox = Tk.Checkbutton(root, text= "Plot Specific Sample Range", variable = sampleRangeVar, onvalue = 1, offvalue = 0)

# Creates buttons for user input.                
button_graph = Tk.Button(master=root, text='Refresh Plot', command = refreshPlot)
button_clear = Tk.Button(master=root, text=' Clear Plot ', command = clearPlot)

# Creates Labels to define the entry boxes.
upperSpecLabel = Tk.Label(root, text="Upper Spec Limit:")
lowerSpecLabel = Tk.Label(root, text="Lower Spec Limit:")
zValueLabel = Tk.Label(root, text="Z Value:")
upperRangeLabel = Tk.Label(root, text = "Upper Sample Range:")
lowerRangeLabel = Tk.Label(root, text = "Lower Sample Range:")

# Creates the entry boxes for user input.
upperSpecEntry = Tk.Entry(root, textvariable = USL)
lowerSpecEntry = Tk.Entry(root, textvariable = LSL)
zValueEntry = Tk.Entry(root, textvariable = zValue)
upperRangeEntry = Tk.Entry(root, textvariable = upperSampleRange)
lowerRangeEntry = Tk.Entry(root, textvariable = lowerSampleRange)

# Creates the label which shows the image / plots.
img = ImageTk.PhotoImage(Image.open("DefaultScreen.png"))
panel = Tk.Label(root, image = img)

# Places all objects in the GUI using the grid layout.
panel.grid(row=0,column=0, columnspan=8,)

button_graph.grid(row=2, column=7, columnspan = 2, sticky=Tk.N+Tk.S+Tk.E+Tk.W)
button_clear.grid(row=3, column=7, columnspan = 2, sticky=Tk.N+Tk.S+Tk.E+Tk.W)

specLimitsBox.grid(row = 2, column = 0, columnspan = 2)
upperSpecLabel.grid(row = 3, column = 0)
lowerSpecLabel.grid(row = 4, column = 0)
upperSpecEntry.grid(row = 3, column = 1)
lowerSpecEntry.grid(row = 4, column = 1)

controlLimitsBox.grid(row = 2, column = 2, columnspan = 2)
zValueLabel.grid(row = 3, column = 2)
zValueEntry.grid(row = 3, column = 3)

sampleRangeBox.grid(row = 2, column = 4, columnspan = 2)
upperRangeLabel.grid(row = 3, column = 4)
lowerRangeLabel.grid(row = 4, column = 4)
upperRangeEntry.grid(row = 3, column = 5)
lowerRangeEntry.grid(row = 4, column = 5)

Tk.mainloop()