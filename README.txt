Statistical Process Control Analysis 1.0
----------------------------------------

Using this Software:
--------------------

This software will take an array of sample data in a .csv file and create process control charts and calculate the control index Cpk.  The chart output will show a raw chart, range chart and mean chart with the x axes representing the sample number and the y axes the measured values.

The software will reference an example data file on startup.  Use the 'Open' function in the 'File' menu to specify a different set of data.

See the file spcDataTemplate.csv for an example of the formatting needed for the input data.


Process Control Charts:
-----------------------

Process control charts are a tool used to monitor a manufacturing process.  The charts provide a visual tool to quickly see how a process is performing and allows for a user to easily see trends or out of control situations.  

This program generates three different control charts: Raw, Range, and Mean.  The raw chart shows all raw data values for each sample taken.  Range shows the range of values for each sample, and mean shows the mean value for each sample.

Specification limits can be added to the charts to compare the measured value to the product specifications.  Control limits can also be added as a number of standard deviations from the population center.


Understanding Cpk:
------------------
Cpk is a process capability index used for analyzing uncentered processes.  The index can be used to easily determine the process health and expected number of defects.

Distribution	Cpk	Expected Defects
3.0 Sigma		0.50	66810 ppm
4.5 Sigma		1.00	2700 ppm
6.0 Sigma		1.50	3.4 ppm


Notes for Python Script:
------------------------

Packages needed to run python scrip main.py:

PIL
Tkinter
numpy
sys
matplotlib
tkFileDialog
tkMessageBox
shutil