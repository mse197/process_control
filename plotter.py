import plotTypes
import matplotlib.pyplot as plt

# Function to determine the minimum value of a chart.
def getMin(LSL, MeanLCL, yMean):
        
        if LSL != False:
            if MeanLCL != False:
                yMin = min(LSL, MeanLCL, min(yMean))
                return yMin

        if LSL == False:
            if MeanLCL != False:
                yMin = min(MeanLCL, min(yMean))
                return yMin
                
        if LSL != False:
            if MeanLCL == False:
                yMin = min(LSL, min(yMean))
                return yMin

        if LSL == False:
            if MeanLCL == False:
                yMin = min(yMean)
                return yMin

# Function to determine the maximum falue of a chart.
def getMax(USL, MeanUCL, yMean):
        
        if USL != False:
            if MeanUCL != False:
                yMin = max(USL, MeanUCL, max(yMean))
                return yMin

        if USL == False:
            if MeanUCL != False:
                yMin = max(MeanUCL, max(yMean))
                return yMin
                
        if USL != False:
            if MeanUCL == False:
                yMin = max(USL, max(yMean))
                return yMin

        if USL == False:
            if MeanUCL == False:
                yMin = max(yMean)
                return yMin


# Function takes input data and passes them to functions in plots.py to create a single figure with mutiple complex plots.
def createPlots(xRaw, yRaw, xMeanRange, yMean, yRange, yMeanCenter, yRangeCenter, MeanUCL, MeanLCL, RangeUCL, cpk, USL, LSL, upperSampleRange, lowerSampleRange):

        USRange = upperSampleRange
        LSRange = lowerSampleRange

        if upperSampleRange == False:
            global USRange
            USRange = len(xMeanRange) + 1
        
        if lowerSampleRange == False:
            global LSRange
            LSRange = 0
        
        
        # Clears previous plots
        plt.clf()
        
        # Creates a subplot which combines all necessary plots to create the Mean Control Chart
        plt.figure(1)
        plt.subplot(311)
        plotTypes.plotMean(xMeanRange, yMean)
        plotTypes.plotCenterLine(xMeanRange, yMeanCenter)
        if MeanUCL != False:
            plotTypes.plotControlLimit(xMeanRange, MeanUCL)
        if MeanLCL != False:
            plotTypes.plotControlLimit(xMeanRange, MeanLCL)
        if USL != False:
            plotTypes.plotSpecLimit(xMeanRange, USL)
        if LSL != False:
            plotTypes.plotSpecLimit(xMeanRange, LSL)
        yMinMean = getMin(LSL, MeanLCL, yMean)
        yMaxMean = getMax(USL, MeanUCL, yMean)
        plt.xlim(LSRange, USRange)
        plt.ylim(yMinMean - 2, yMaxMean + 2)

        # Creates a subplot which combines all necessary plots to create the Range Control Chart
        plt.subplot(312)
        plotTypes.plotRange(xMeanRange, yRange)
        plotTypes.plotCenterLine(xMeanRange, yRangeCenter)
        if RangeUCL != False:
            plotTypes.plotControlLimit(xMeanRange, RangeUCL)
        plt.xlim(LSRange, USRange)
        
        # Creates a subplot which combines all necessary plots to create the Raw Control Chart
        plt.subplot(313)
        plotTypes.plotRaw(xRaw,xMeanRange,yRaw)
        plotTypes.plotCenterLine(xMeanRange, yMeanCenter)
        if MeanUCL != False:
            plotTypes.plotControlLimit(xMeanRange, MeanUCL)
        if MeanLCL != False:
            plotTypes.plotControlLimit(xMeanRange, MeanLCL)
        if USL != False:
            plotTypes.plotSpecLimit(xMeanRange, USL)
        if LSL != False:
            plotTypes.plotSpecLimit(xMeanRange, LSL)
        yMinRaw = getMin(LSL, MeanLCL, yRaw)
        yMaxRaw = getMax(USL, MeanUCL, yRaw)      
        plt.xlim(LSRange, USRange)
        plt.ylim(yMinRaw - 2, yMaxRaw + 2)
        if cpk != False:
            plt.figtext(0.8, 0.01,"Cpk = "+ str('%.2f'%(cpk)))

        plt.tight_layout()
        plt.savefig("workingFile.png")
        
     