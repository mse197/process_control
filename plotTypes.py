import matplotlib.pyplot as plt
from numpy import arange

# Creates a scatter plot of the raw data for each sample.
def plotRaw(x,xmean,y):
        
        plt.scatter(x,y)
        plt.title('Raw Control Chart')
        plt.xlabel('Sample')
        plt.ylabel('Test Value')
	plt.xlim(0,len(x)/(len(x)/len(xmean)) + 1)

# Creates the plot of the mean values for each sample.
def plotMean(x, y):
        
        plt.plot(x,y,'b')
        plt.title('Mean Control Chart')
        plt.xlabel('Sample')
        plt.ylabel('Test Value')
	plt.xlim(0,len(x) + 1)

# Creates the plot of the range values for each sample	
def plotRange(x, y):
        
        plt.plot(x,y,'b')
        plt.title('Range Control Chart')
        plt.xlabel('Sample')
        plt.ylabel('Test Value')
	plt.xlim(0, len(x) + 1)

# Plots a solid red control limit
def plotControlLimit(x,controlLimit):
    
        controlRange = arange(1, len(x), 0.1)
        function = (0 * controlRange) + controlLimit
        plt.plot(controlRange, function, 'r')

# Plots a dashed green center line
def plotCenterLine(x,centerLine):
    
        centerRange = arange(1, len(x), 0.1)
        function = (0 * centerRange) + centerLine
        plt.plot(centerRange, function, 'g--')
        
# Plots a solid black spec limit
def plotSpecLimit(x, specLimit):
    
        specRange = arange(1, len(x), 0.1)
        function = (0 * specRange) + specLimit
        plt.plot(specRange, function, 'k')