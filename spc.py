import calculator
import plotter

# Initalizes variables needed to start GUI
data = calculator.getArray('spcDataTemplate.csv')
z = 1.5
upperSpec = 0
lowerSpec = 0
upperSampleRange = False
lowerSampleRange = False
    
# Defines variables and stores variables needed for plotting.
xRaw, yRaw, xMeanRange, yMean, yRange, yMeanCenter, yRangeCenter, MeanUCL, MeanLCL, RangeUCL, Cpk = calculator.getAllValues(data, z, upperSpec, lowerSpec)

def runControl():
    global yRaw, xMeanRange, yMean, yRange, MeanUCL, MeanLCL, RangeUCL, z
    MeanUCL = calculator.getUCL(xMeanRange, yMean, yRaw, z)
    MeanLCL = calculator.getLCL(xMeanRange, yMean, yRaw, z)
    RangeUCL = calculator.getRangeUCL(xMeanRange, yMean, yRange, z)
    
def runVars():
    # Calls for lists for all need values for plotting the control charts
    global xRaw, yRaw, xMeanRange, yMean, yRange, yMeanCenter, yRangeCenter, MeanUCL, MeanLCL, RangeUCL, Cpk
    xRaw, yRaw, xMeanRange, yMean, yRange, yMeanCenter, yRangeCenter, MeanUCL, MeanLCL, RangeUCL, Cpk = calculator.getAllValues(data, z, upperSpec, lowerSpec)
    

def runPlots():
    # Plots control charts based on lists previously created.
    global xRaw, yRaw, xMeanRange, yMean, yRange, yMeanCenter, yRangeCenter, MeanUCL, MeanLCL, RangeUCL, Cpk
    plotter.createPlots(xRaw, yRaw, xMeanRange, yMean, yRange, yMeanCenter, yRangeCenter, MeanUCL, MeanLCL, RangeUCL, Cpk, upperSpec, lowerSpec, upperSampleRange, lowerSampleRange)
