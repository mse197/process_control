from numpy import mean
from numpy import std
from numpy import isnan
from numpy import genfromtxt
from numpy import delete

# Reads the data file into an array of data which can be passed to other functions.
def getArray(filename):
		
	dataArray = genfromtxt(filename, delimiter=',')
	dataArray = delete(dataArray, 0, 0)

	return dataArray

# Reads file from input and creates a list representing the x values for the raw data.
def getXraw(data):
              
        xList = []        
        row = 0
       
        for num in range(data[1:,0].size+1):
                column = 1                        
                for num in range(data[row,1:].size): 
                        if isnan(float(data[row,column])) == True:
                            column += 1
                        else:                                         
                            xList.append(int(data[row,:1]))
                            column += 1           
                row += 1
                                   
        return xList
        
# Reads file from input and creates a list representing the y values for the raw data.
def getYraw(data):
    
        yRaw = []        
        row = 0
        
        for num in range(data[1:,0].size+1):                
                column = 1                          
                for num in range(data[row,column:].size):                    
                        if isnan(float(data[row,column])) == True:
                            column += 1
                        else:                        
                            yRaw.append(float(data[row,column]))                        
                            column += 1                    
                row += 1
                
        return yRaw
        
# Reads file from input and creates a list representing the x values for the mean and range data.     
def getXmean_range(data):
              
        xMean = []        
        row = 0
       
        for num in range(data[1:,0].size+1):          
                xMean.append(int(data[row,:1]))                
                row += 1
                
                                                        
        return xMean

# Reads file from input and creates a list representing the y values for the mean data.
# Cannot use numpy.mean() as this will not exclude for 'nan' values in the data set.       
def getYmean(data):
    
        yMean = []        
        row = 0
        column = 1
        sumElement = float(0)
        count = 0
        
        for num in range(data[0:,0].size):

            for num in range(data[row,column:].size):
                if isnan(data[row,column]) == False:         
                   sumElement = sumElement + float(data[row,column])
                   count += 1
                   column += 1
                else:
                    column +=1
                    
            yMean.append(sumElement / count)
            column = 1
            count = 0                     
            row += 1
            sumElement = float(0)

        return yMean


# Reads file from input and creates a list representing the y values for the range data.
def getYrange(data):
    
        yRange = []        
        row = 0
        column = 1
        
        for num in range(data[1:,0].size+1):
                
                yRange.append(float(max(data[row,column:]) 
                    - min(data[row, column:])))                 
                row += 1

        return yRange

# Reads input of Y list of values and returns a list of the mean value for the centerline.
def getYCenter(yList):
	
	meanValue = float(mean(yList))

	return meanValue

# Calculates the upper control limit of the population using the raw data using the xMean and yRaw data.
def getUCL(xMean, yMean, yRaw, z):

	ySTD = std(yRaw)
	limit = mean(yMean) + (z * ySTD)

	return limit

#Calculates the lower control limit of the population using the raw data using the xMean, yMean, yRaw and z value.
def getLCL(xMean, yMean, yRaw, z):

        ySTD = std(yRaw)
        limit = mean(yMean) - (z * ySTD)

        return limit
        
# Calculates the upper control limit for the range using the raw data using the xMean and yRaw data.
def getRangeUCL(xMean, yMean, yRange, z):

	ySTD = std(yRange)
	limit = mean(yRange) + (z * ySTD)

	return limit
	
# Calculates the Cpk value for the process
def getCpk(yRaw, yMeanCenter, upperSpec, lowerSpec):
              
        if upperSpec != False:    
            cpk = min(((upperSpec - yMeanCenter)/(3*std(yRaw))),((yMeanCenter - lowerSpec)/(3*std(yRaw))))
        
            return cpk
        
        else:
            return False

# Runs functions within calculator.py to return all the needed lists and values for plotting in a single function.
def getAllValues(data, z, upperSpec, lowerSpec):
        
        # Variables are defined using functions which create specific lists based off the raw input.
        xRaw = getXraw(data)
        yRaw = getYraw(data)
        xMeanRange = getXmean_range(data)
        yMean = getYmean(data)
        yRange = getYrange(data)
        yMeanCenter = getYCenter(yMean)
        yRangeCenter = getYCenter(yRange)
        MeanUCL = getUCL(xMeanRange, yMean, yRaw, z)
        MeanLCL = getLCL(xMeanRange, yMean, yRaw, z)
        RangeUCL = getRangeUCL(xMeanRange, yMean, yRange, z)
        Cpk = getCpk(yRaw, yMeanCenter, upperSpec, lowerSpec)
        
        return xRaw, yRaw, xMeanRange, yMean, yRange, yMeanCenter, yRangeCenter, MeanUCL, MeanLCL, RangeUCL, Cpk